package com.example.recipes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;


public class femRecipe extends AppCompatActivity {
    int recipe;
    ArrayList<String> countries = new ArrayList<String>();
    ArrayList<String> Texts= new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        recipe = intent.getIntExtra("selectedItem",recipe);
        setContentView(R.layout.recipe);
        TextView namedtext = findViewById(R.id.namedText);
        TextView textView = findViewById(R.id.textView);
        EditText editText = findViewById(R.id.editTextNumber);
        Button button = findViewById(R.id.button);
        try {
            InputStream inputreader = getAssets().open("femnameText.txt");
            Scanner scanner = new Scanner(inputreader);

            while(scanner.hasNextLine())
            {
                countries.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String selectedItem = (String) countries.get(recipe);
        namedtext.setText(selectedItem);
        switch (recipe){
            case 0:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 5.5)&(result< 5.8)){
                            textView.setText("Ваша оценка 3");
                        }else if((result >= 5.1) & (result<=5.6)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=5){
                            textView.setText("Ваша оценка 5");
                        }else if(result>5.7) {
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 1:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 10.1)&(result< 10.5)){
                            textView.setText("Ваша оценка 3");
                        }else if((result >= 9.4) & (result<=10.1)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=9.3){
                            textView.setText("Ваша оценка 5");
                        }else if(result>10.5){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 2:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 17.2)&(result< 17.6)){
                            textView.setText("Ваша оценка 3");
                        }else if((result > 16) & (result<=17.2)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=16){
                            textView.setText("Ваша оценка 5");
                        }else if(result>17.6){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 3:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 11.20)&(result< 12.0)){
                            textView.setText("Ваша оценка 3");
                        }else if((result > 9.50) & (result<=11.20)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=9.50){
                            textView.setText("Ваша оценка 5");
                        }else if(result>12.0){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 4:
                button.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 13)&(result> 11)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 19) & (result>=13)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=19){
                            textView.setText("Ваша оценка 5");
                        }else if(result<11){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 5:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 11)&(result> 9)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 16) & (result>=11)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=16){
                            textView.setText("Ваша оценка 5");
                        }else if(result<9){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 6:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 9)&(result> 7)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 16) & (result>=9)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=16){
                            textView.setText("Ваша оценка 5");
                        }else if(result<7){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 7:
                    button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 170)&(result> 160)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 185) & (result>=170)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=185){
                            textView.setText("Ваша оценка 5");
                        }else if(result<160){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
        }
    }
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("mainText")) {
            recipe = savedInstanceState.getInt("selectedItem");
        }
    }
}