package com.example.recipes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;


public class Recipe extends AppCompatActivity {
    int recipe;
    ArrayList<String> countries = new ArrayList<String>();
    ArrayList<String> Texts= new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        recipe = intent.getIntExtra("selectedItem",recipe);
        setContentView(R.layout.recipe);
        TextView namedtext = findViewById(R.id.namedText);
        TextView textView = findViewById(R.id.textView);
        EditText editText = findViewById(R.id.editTextNumber);
        Button button = findViewById(R.id.button);
        try {
            InputStream inputreader = getAssets().open("nameText.txt");
            Scanner scanner = new Scanner(inputreader);

            while(scanner.hasNextLine())
            {
                countries.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String selectedItem = (String) countries.get(recipe);
        namedtext.setText(selectedItem);
        switch (recipe){
            case 0:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 4.7)&(result<= 5)){
                            textView.setText("Ваша оценка 3");
                        }else if((result >= 4.4) & (result<=4.7)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=4.4){
                            textView.setText("Ваша оценка 5");
                        }else if(result>4.9){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 1:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 8.5)&(result<= 8.8)){
                            textView.setText("Ваша оценка 3");
                        }else if((result >= 8.1) & (result<=8.5)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=8){
                            textView.setText("Ваша оценка 5");
                        }else if(result>8.8){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 2:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if((result > 14.3)&(result<= 14.6)){
                            textView.setText("Ваша оценка 3");
                        }else if((result > 13.4) & (result<=14.3)){
                            textView.setText("Ваша оценка 4");
                        }else if(result<=13.4){
                            textView.setText("Ваша оценка 5");
                        }else if(result>14.6){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 3:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        float result = Float.parseFloat(line);
                        if ((result > 14.30) & (result <= 15)) {
                            textView.setText("Ваша оценка 3");
                        } else if ((result > 12.40) & (result <= 14.30)) {
                            textView.setText("Ваша оценка 4");
                        } else if (result <= 12.40) {
                            textView.setText("Ваша оценка 5");
                        } else if (result > 15) {
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 4:
                button.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 11)&(result>= 9)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 14) & (result>=11)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=14){
                            textView.setText("Ваша оценка 5");
                        }else if(result<9){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 5:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 31)&(result>= 27)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 42) & (result>=31)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=42){
                            textView.setText("Ваша оценка 5");
                        }else if(result<27){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 6:
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 8)&(result>= 6)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 13) & (result>=8)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=13){
                            textView.setText("Ваша оценка 5");
                        }else if(result<6){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
            case 7:
                    button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view) {
                        String line = editText.getText().toString();
                        int result = Integer.parseInt(line);
                        if((result < 210)&(result>= 195)){
                            textView.setText("Ваша оценка 3");
                        }else if((result < 230) & (result>=210)){
                            textView.setText("Ваша оценка 4");
                        }else if(result>=230){
                            textView.setText("Ваша оценка 5");
                        }else if(result<195){
                            textView.setText("Ваша оценка 2");
                        }
                    }
                });
                break;
        }
    }
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("mainText")) {
            recipe = savedInstanceState.getInt("selectedItem");
        }
    }
}