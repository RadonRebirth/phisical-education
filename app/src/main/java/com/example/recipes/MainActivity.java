package com.example.recipes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_main);
        CheckBox femaleCheck = findViewById(R.id.checkBox);
        CheckBox maleCheck = findViewById(R.id.checkBox2);
        Button button = findViewById(R.id.button3);
        button.setEnabled(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(femaleCheck.isChecked()) {
                    Intent intent = new Intent(MainActivity.this, femActivity.class);
                    startActivity(intent);
                }else if(maleCheck.isChecked()){
                    Intent intent = new Intent(MainActivity.this, maleActivity.class);
                    startActivity(intent);
                }
            }
        });
        femaleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maleCheck.setChecked(false);
                button.setEnabled(true);
            }
        });
        maleCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                femaleCheck.setChecked(false);
                button.setEnabled(true);
            }
        });
    }
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
